<#
.SYNOPSIS
Get physical network adapters
.EXAMPLE
Get-PhysicalAdapters -ComputerName localhost
#>

[CmdletBinding()]
param (
  [Parameter(Mandatory = $true)]
  [Alias('hostname')]
  $computerName = 'localhost'
)

Write-Verbose "Connecting to $computerName"

Get-WmiObject Win32_networkAdapter -ComputerName $computerName |
Where-Object { $_.PhysicalAdapter } |
Select-Object MACAddress, AdapterType, DeviceID, Name, Speed

Write-Verbose "Finished running command"
