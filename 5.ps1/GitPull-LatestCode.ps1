﻿# Powershell "&'D:\000 - mon\210 - coding\python-scripts-for-myself\5.ps1\GitPull-LatestCode.ps1'"

Write-Host "开始从仓库拉去最新的代码:" -ForegroundColor Green

$currentPath = Get-Location

[string]$workspaceDirPath = "D:\capdiem\110 qst"
$dirs = Get-ChildItem $workspaceDirPath -Directory

foreach ($dir in $dirs) {
  $path = $dir.FullName
  Write-Host $path -ForegroundColor Yellow

  $hidden = Get-ChildItem $path -Hidden | Select-Object -ExpandProperty Name
  if ($null -ne $hidden) {
    if ($hidden.Contains('.git')) {
      Set-Location $path
      git pull
    }
  }
}

Set-Location $currentPath

Write-Host "拉取完毕！" -ForegroundColor Green