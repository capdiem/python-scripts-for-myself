# Set-PSReadlineOption EditMode to windows
Set-PSReadLineOption -EditMode Windows
# Set-PSReadLineKeyHandler
Set-PSReadLineKeyHandler -Key Ctrl+. -Function MenuComplete


function Set-Prompt {
  param(
    [Parameter(Mandatory = $true)]
    [ValidateSet('C', IgnoreCase = $true)]
    $Mode
  )

  $varPSPromptMode = (Get-Variable 'PSPromptMode'  -ea SilentlyContinue)

  #配置变量不存在则新建
  if ($null -eq $varPSPromptMode) {
    New-Variable -Name 'PSPromptMode' -Value $Mode -Scope 'Global'
    $varPSPromptMode = Get-Variable -Name 'PSPromptMode'

    $varPSPromptMode.Description = '提示函数配置变量'

    #限制配置变量的取值集合
    $varPSPromptModeAtt = New-Object System.Management.Automation.ValidateSetAttribute('C')
    $varPSPromptMode.Attributes.Add($varPSPromptModeAtt)

    #限制配置变量为只读并且可以贯穿所有作用域ls
    $varPSPromptMode.Options = 'ReadOnly, AllScope'
  }

  #更新配置
  #只读变量可以通过-force选项更新值
  Set-Variable -Name PSPromptMode -Value $Mode -Force
}

## 默认控制台提示执行函数
function Prompt() {
  switch ($PSPromptMode) {
    'c' {
      $color = 'Magenta'
      $prompt = ''
      $location = $executionContext.SessionState.Path.CurrentLocation.Path

      if ($location -eq $HOME) {
        $prompt = "~"
      }
      else {
        $arr = $location.Split("/")
        $length = $arr.Length
        if ($length -eq 2 -and $arr[1] -eq "") {
          $prompt = "/"
        }
        elseif ($location.EndsWith("\")) {
          # 注册表 $location 是以 \ 结尾的
          $prompt = $arr[$length - 2]
        }
        else {
          $prompt = $arr[$length - 1]
        }
      }

      $action = Write-Host $prompt -NoNewLine -ForegroundColor $color

      $action += Write-Host "$(" м")$(":" * ($nestedPromptLevel + 1))" -NoNewLine -ForegroundColor $color

      "$action "
    }
    Default {
      "PS $($executionContext.SessionState.Path.CurrentLocation)$('>' * ($nestedPromptLevel + 1)) "
    }
  }
}

# set prompt "C"
Set-Prompt C


#region aliases

Set-Alias gcmd Get-Command

#endregion
