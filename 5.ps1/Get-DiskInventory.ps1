<#
.SYNOPSIS
Get-DiskInventory retrieves logical disk information from one or more computers.
.DESCRIPTION
Get-DiskInventory uses WMI to retrieve the Win32_LogicalDisk instances from one or more computers. It displays each disk's drive letter, free space, total size, and percentage of free space.
.PARAMETER computerName
The computer name, or names, to query. Default: Localhost.
.PARAMETER driveType
The drive type to query. See Win32_LogicalDisk documentation for values. 3is a fixed disk, and is the default.
.EXAMPLE
Get-DiskInventory -computerName localhost driveType 3
#>

[CmdletBinding()]
param (
  [Parameter(Mandatory = $true, HelpMessage = "Enter a computer name to query")]
  [Alias('hostname')]
  [string]$computerName,
  [ValidateSet(2, 3)] # 3:本地磁盘 2:移动磁盘
  [int]$driveType = 3
)

if ($PSEdition -ne "core")
{
  Write-Verbose "Connecting to $computerName"
  Write-Verbose "Looking for drive type $driveType"

  Get-WmiObject -Class Win32_LogicalDisk `
    -ComputerName $computerName `
    -Filter "driveType=$driveType" |
  Sort-Object -Property DeviceID |
  Select-Object -Property DeviceID,
  @{label = 'FreeSpace(MB)'; expression = { $_.FreeSpace / 1MB -as [int] } },
  @{label = 'Size(GB)'; expression = { $_.Size / 1GB -as [int] } },
  @{label = '%Free'; expression = { $_.FreeSpace / $_.Size * 100 -as [int] } }

  Write-Verbose "Finished running command"
}
else
{
  Write-Warning "Sorry! Powershell core can't support Get-WmiObject cmdlet"
}
