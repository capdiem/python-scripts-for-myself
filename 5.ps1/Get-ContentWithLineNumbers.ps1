param (
  [Parameter(Mandatory=$true)]
  [string[]]$Path,
  [switch]$NoLine
)

if (Get-Command bat -errorAction SilentlyContinue) {
  if ($NoLine) {
    Get-Content $Path
  }
  else {
    bat.ps1 $Path
  }
}
else {
  Get-Content $Path
}