function Run-Git-Add() {
  local arg=${1:=.}
  git add $arg
}

function Run-Git-Commit() {
  if [ "$1" -eq "" ]
  then
    echo "msg can't be empty!"
  elif [[ "$1" =~ ^-. ]]
  then
    git commit $1
  elif [[ "$1" =~ (支持|新增|增加|添加|实现|add|初始化|support|use) ]]
  then
    git commit -m "🌸$1"
  elif [[ "$1" =~ (修复|解决|fix|bug|除虫|resovle) ]]
  then
    git commit -m "🐛$1"
  elif [[ "$1" =~ (优化|改进|增强|统一|update|调整|更新|optimization|set) ]]
  then
    git commit -m "🚀$1"
  elif [[ "$1" =~ (删除|remove|delete|移除|去除|drop) ]]
  then
    git commit -m "🤦$1"
  else
    git commit -m "👾$1"
  fi
}

function Run-Git-Log() {
  local arg=${1:=7}
  git log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit -$arg
}

alias ga=Run-Git-Add
alias gcm=Run-Git-Commit
alias gh=Run-Git-Log
alias gs='git status'
alias gpl='git pull'
alias gck='git checkout'
alias gps='git push'
alias gb='git branch'