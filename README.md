﻿# Scripts for Myself

> 记录一些自用的脚本

## Python

### 功能

1. 抓取网易云音乐每日歌单部分信息
   - 支持将抓取结果输出到文件（增量）
   - 支持输入你的用户名查找
2. 每周电影困难选择症
   - 支持输入你的想看链接，但mode必须为list，例如[我的想看地址](https://movie.douban.com/people/carpe-diem1018/wish?sort=time&amp;start=0&amp;filter=all&amp;mode=list&amp;tags_sort=count)
   - 虽然标题是电影主题，但我读、我听、我的舞台剧都可以爬
3. 工作中用到的一些`python`脚本，目标主要是一些从数据库（MSSQL、MongoDB）中查询数据并导出为 excel 的功能。

### 使用

```powershell
# use scoop in Windows or use brew in Mac
scoop install python
# python installed, pip installed too
pip install beautifulsoup4
pip install selenium
scoop install geckodriver
# after git clone this project
cd .\1.抓取网易云音乐每日歌单部分信息\
python .\script.py
```

![抓取网易云音乐每日歌单部分信息](https://images.gitee.com/uploads/images/2018/0805/230641_05170b10_1528203.gif "抓取网易云音乐每日歌单部分信息")

```powershell
cd .\2.每周电影困难选择症\
python .\script.py
```

![每周电影困难选择症](https://images.gitee.com/uploads/images/2018/0805/230659_e60f92c7_1528203.gif "每周电影困难选择症")

***

## Autohotkey

> 会根据自己需求不断添加和完善自定义热键

### 功能

1. ```Win+` ```: 显示或隐藏 Powershell，如果没有启动则启动。
2. `Win+E`: 显示或隐藏资源管理器，如果没有启动则启动 。**原热键功能只是新建资源管理器。**
3. `Ctrl+T`: 辅助以上热键。如果当前活动窗口为 Powershell 或资源管理器则新建同样的窗口。
4. `Ctrl+Tab`: 辅助以上热键。如果当前活动窗口为 Powershell 或资源管理器则激活同类窗口的下一个窗口。**不能按照顺序》 <。**
5. `Win+W`: 最小化当前活动窗口。**此热键会覆盖原热键功能。**
6. `Win+Q`: 关闭当前活动窗口。相当于```Ctrl+F4```。**此热键会覆盖原热键功能。**
7. `Ctrl+Win+WheelUp` / `Ctrl+Win+WheelDown`: 左右切换虚拟桌面，相当于`win+ctrl+left`和`win+ctrl+right`。
8. `Win+T` / `鼠标中键`: 置顶当前活动窗口。
9. 鼠标左键拖动窗口会同时改变窗口透明度（如果存在黑边和闪烁的窗口，需要维护到代码里去，例如`Snipaste`和`企业QQ`等）。

### 使用

1. 下载 [Autohotkey](https://www.autohotkey.com/download/) 并安装
2. 使用 git clone 本项目源码，或者直接点击本页的下载源文件解压
3. 双击运行 hotkey.ahk 文件即运行，运行时任务栏标签存在 Autohotkey 图标
4. 也可把该文件复制到 ```C:\Users\[你的用户名]\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup``` 文件夹下，每次启动电脑会自动运行 hotkey.ahk 脚本

***

## Powershell

### 功能

1. 封装了一些在日常工作中高频使用的`git`命令。
2. 修改默认控制台提示风格
3. 修改默认 cat 输出，使用 bat 输出。

### 使用

```powershell
# 1.打开powershell，使用notepad打开默认起始文件
notepad $PROFILE
# 2.将脚本内容复制进去保存
# 3.重新启动powershell，会读取文件内容
powershell
# v6.* 使用 pwsh
```
