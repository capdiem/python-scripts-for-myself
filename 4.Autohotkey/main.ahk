﻿global powershell_exe_path := "D:\scoop\shims\pwsh.exe"
global cfosspeed_spd_exe_path := "C:\Program Files\cFosSpeed\spd.exe"

; 仅在资源管理器窗口活动时有效：ctrl+t 执行RunExplorerLabel
Hotkey, IfWinActive, ahk_class CabinetWClass
  Hotkey, ^t, NewExplorerLabel
HotKey, ^w, CloseWindowLabel
HotKey, ^Tab, NextExplorerLabel

; 仅在powershell窗口活动时有效：ctrl+t 执行NewPSLabel
Hotkey, IfWinActive, ahk_exe pwsh.exe
  Hotkey, ^t, NewPSLabel
HotKey, ^w, CloseWindowLabel
HotKey, ^Tab, NextPowershellLabel

; 仅在csgo窗口活动时有效：LWin 执行DisabledWinLabel
Hotkey, IfWinActive, ahk_exe csgo.exe
  HotKey, LWin, DisabledWinLabel

GroupAdd, Explorers, ahk_class CabinetWClass	; 添加所有资源管理器窗口到Explorers组里
GroupAdd, Powershells, ahk_exe pwsh.exe	; 添加所有powershell窗口到Powershells组里

; 任何想在脚本启动时立即执行的行都应该出现在脚本的顶部, 要在首个热键, 热字串或 Return 之前

; **************************************************************************************************
; << Hotkey 需要在定义热键前定义
; **************************************************************************************************

; win + `: 显示或隐藏powershell
#`::
  ; if WinExist("ahk_group Powershells") {
  ;   If (!WinActive("ahk_group Powershells")) {
  ;     WinActivate
  ;   } else {
  ;     WinMinimize
  ;   }
  ; } else {
  ;   Run %powershell_exe_path%
  ; }

  if WinExist("ahk_exe WindowsTerminal.exe") {
    If WinActive("ahk_exe WindowsTerminal.exe") {
      WinMinimize
    } else {
      WinActivate
    }
  } else {
    Run shell:AppsFolder\Microsoft.WindowsTerminalPreview_8wekyb3d8bbwe!App
    ; Run *RunAs shell:AppsFolder\Microsoft.WindowsTerminal_8wekyb3d8bbwe!App
  }
return

; win + q: 关闭应用程序
#q::
  WinClose, A	; A:活动窗口
return

; win + e: 覆盖原win+e新建资源管理器功能 显示或隐藏资源管理器
#e::
  if WinExist("ahk_group Explorers") {
    if (!WinActive("ahk_group Explorers")) {
      WinActivate
      #WinActivateForce	; 强制激活，也许可以阻止在快速连续激活窗口时任务栏按钮的闪烁
      WinGetPos, x, y	; 获取当前窗口的位置
      WinMove, x-0.01, y	; 显示位置后移到一点 以唤醒tidytabs(配合tidytabs使用)
    } else {
      WinMinimize
    }
  } else {
    Run explorer.exe
  }
return

; win + w: 最小化当前窗口
#w::
  WinMinimize, A ; A:活动窗口
return

; win + t | middle button: 置顶当前窗口
global always_on_top_id :=	;当前置顶的窗口id
global always_on_top_original_transparent :=	;置顶窗口原始的透明度
#t::
~MButton::
  MouseGetPos, , , active_id
  WinGetClass, active_class, ahk_id %active_id%

  ; 排除桌面
  If (active_class = "Progman")
    return

  WinGet, active_minmax, MinMax, ahk_id %active_id%
  WinGet, active_transparent, Transparent, ahk_id %active_id%
  if (always_on_top_id == "") {
    ; 置顶
    if (active_minmax == 1)
      return ; 最大化窗口禁止置顶
    always_on_top_original_transparent := active_transparent
    WinSet, Transparent, 155, ahk_id %active_id%
    WinSet, AlwaysOnTop, On, ahk_id %active_id%
    always_on_top_id := active_id
  } else if (always_on_top_id != active_id) {
    ; 已存在置顶时 置顶另一个窗口
    if (active_minmax == 1)
      return ; 最大化窗口禁止置顶
    if (always_on_top_original_transparent == "") {
      WinSet, Transparent, 255, ahk_id %always_on_top_id%
    } else {
      WinSet, Transparent, %always_on_top_original_transparent%, ahk_id %always_on_top_id%
    }
    WinSet, AlwaysOnTop, Off, ahk_id %always_on_top_id%
    WinSet, Transparent, 155, ahk_id %active_id%
    WinSet, AlwaysOnTop, On, ahk_id %active_id%
    always_on_top_id := active_id
    always_on_top_original_transparent := active_transparent
  } else {
    ; 取消置顶
    If (always_on_top_original_transparent == "") {
      WinSet, Transparent, 255, ahk_id %active_id%
    } else {
      WinSet, Transparent, %always_on_top_original_transparent%, ahk_id %active_id%
    }
    WinSet, AlwaysOnTop, Off, ahk_id %active_id%
    always_on_top_id := ""
    always_on_top_original_transparent := ""
  }
return

;
+WheelUp::
  MouseGetPos, , , active_id
  WinGetClass, active_class, ahk_id %active_id%

  ; 排除桌面
  If (active_class = "Progman")
    return

  WinGet active_transparent, Transparent, ahk_id %active_id%
  If (active_transparent = "")
    active_transparent = 255

  active_transparent := active_transparent + 15
  If (active_transparent > 255)
    active_transparent := 255

  WinSet, Transparent, %active_transparent%, ahk_id %active_id%
return

;
+WheelDown::
  MouseGetPos, , , active_id
  WinGetClass, active_class, ahk_id %active_id%

  ; 排除桌面
  If (active_class = "Progman")
    return

  WinGet active_transparent, Transparent, ahk_id %active_id%
  If (active_transparent = "")
    active_transparent = 255

  active_transparent := active_transparent - 15
  If (active_transparent < 100)
    active_transparent := 100


  WinSet, Transparent, %active_transparent%, ahk_id %active_id%
return

; 当用户拖动窗体时将活动窗体透明
~LButton::
  exclude_win_exe_list := "Snipaste.exe,QQEIM.exe,TIM.exe,QQ.exe,Thunder.exe,GifCam.exe,DropIt.exe,QiDian.exe,cfosspeed.exe,360AP.exe,kwifi.exe" ;排除APP列表 否则会出现黑边和闪烁
  exclude_win_class_list :="Shell_TrayWnd,Microsoft.IME.UIManager.CandidateWindow.Host,DesktopLyrics"

  moved := false ;移动

  MouseGetPos, , , win_id	;获取当前鼠标所在窗口的id
  WinGet, win_exe, ProcessName, ahk_id %win_id%
  WinGetTitle, win_title, ahk_id %win_id%
  WinGetClass, win_class, ahk_id %win_id%

  ; 在排除列表则不向下执行
  If win_exe In %exclude_win_exe_list%
    return

  ; 没有标题的窗口可能是菜单栏 一律排除
  If (win_title == "")
    return

  ; 窗口类名中存在`popup|tip`的可能是提示弹窗或菜单栏 一律排除
  If (InStr(win_class, "popup", false) || InStr(win_class, "tip", false))
    return
  If win_class In %exclude_win_class_list%
    return

  WinGetPos, win_x, win_y, , , ahk_id %win_id%
  WinGet, win_transparent, Transparent, ahk_id %win_id%
  while GetKeyState("LButton")
  {
    WinGet, active_id, ID, A
    ; 如果窗口id不一样则不向下执行
    If (active_id != win_id)
      break

    WinGetPos, active_x, active_y, , , ahk_id %active_id%
    ; 窗口移动了 设置透明度
    If (win_x != active_x Or win_y != active_y) {
      moved := True
      WinSet, Trans, 128, ahk_id %active_id%
    } else {
      moved := False
    }
    Sleep, 100 ;毫秒
  }
  ; while else
  ; 最小化、最大化和关闭窗口之后 活动窗口会自动跳到上一个窗口
  ; 如果不return会把剩余操作作用到上一个窗口上
  if !WinActive("ahk_id" . win_id) {
    WinGetTitle, current_win_title, A
    WinGetClass, current_win_class, A
    ; 鼠标移动到边缘时 活动窗口可能不存在或者在"贴靠助手"上 此时发生的是 —— **被迫性还原**
    If (current_win_title = "" || current_win_class = "MultitaskingViewFrame")
      RecoverDraggedWindow(win_transparent, win_id)
    return
  }

  ; 移动完毕 —— **主动性还原**、
  If (moved) {
    RecoverDraggedWindow(win_transparent, win_id)
  }
return

; ctrl + win + 鼠标滚轮： 切换虚拟桌面
^#WheelUp::
  Send {LWin Down}{Ctrl Down}{Left}{Ctrl Up}{LWin Up}
return
^#WheelDown::
  Send {LWin Down}{Ctrl Down}{Right}{Ctrl Up}{LWin Up}
return

; cFosSpeed turn on/off favor ping time by command 'spd gset latency {value}'
; 1 - favour pingtime
; 2 - favour bandwith
>!+1::
  if FileExist(cfosspeed_spd_exe_path)
    Run %ComSpec% /c ""%cfosspeed_spd_exe_path%" gset latency 1"",,hide
  ToolTip, favour pingtime
  SetTimer, RemoveToolTip, -500
return

>!+2::
  if FileExist(cfosspeed_spd_exe_path)
    Run %ComSpec% /c ""%cfosspeed_spd_exe_path%" gset latency 2"",,hide
  ToolTip, favour bandwith
  SetTimer, RemoveToolTip, -1000
return

;***************************************************************************************************
; LABELS >>
; **************************************************************************************************

; [LABEL]: 新建资源管理器
NewExplorerLabel:
  If WinExist("ahk_exe TidyTabs.Daemon.exe") {
    Send ^#t
  } else {
    Run explorer.exe
  }
return

; [LABEL]: 激活下一个资源管理器
NextExplorerLabel:
  If WinExist("ahk_exe TidyTabs.Daemon.exe") {
    Send ^#{PgDn}
  } else {
    GroupActivate, Explorers
  }
Return

; [LABEL]: 新建Powershell
NewPSLabel:
  If WinExist("ahk_exe TidyTabs.Daemon.exe") {
    Send ^#t
  } else {
    Run %powershell_exe_path%
  }
return

; [LABEL]: 激活下一个powershell
NextPowershellLabel:
  If WinExist("ahk_exe TidyTabs.Daemon.exe") {
    Send ^#{PgDn}
  } else {
    GroupActivate, Powershells
  }
return

; [LABEL]: close current window
CloseWindowLabel:
  WinClose, A
return

; [LABEL]: 禁止win键
DisabledWinLabel:
return

; [LABEL]: Remove Tooltip
RemoveToolTip:
  ToolTip
return

; **************************************************************************************************
; Functions >>
; **************************************************************************************************
RecoverDraggedWindow(trans, winId) {
  If (trans == "") {
    WinSet, Trans, Off, ahk_id %winId%
  } else {
    WinSet, Trans, %trans%, ahk_id %winId%
  }
}
