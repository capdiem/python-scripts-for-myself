import xlwt
import json
import os


def readFromFile(path):
    file = open(path, 'rb')
    data = json.load(file)
    return data


def getColumnNames(data):
    return list(tuple(data.keys()))


def writeToFile(header, content):
    book = xlwt.Workbook()
    sheet = book.add_sheet('Sheet1', cell_overwrite_ok=True)

    for i in range(len(header)):
        sheet.write(0, i, header[i])

    for i in range(len(content)):
        row = content[i]
        col_values = [row[key] for key in header]
        for col_index in range(len(col_values)):
            sheet.write(1+i, col_index, col_values[col_index])

    book.save('output.xls')


# 没做任何判断 防止如非excel文件错误、没有内容错误
print("请输入Excel文件路径：")
temp = input()
if temp:
    content = readFromFile(temp)
    header = getColumnNames(content[0])
    writeToFile(header, content)
