from pymongo import MongoClient

def getCollection(conn_str, db_name, collection_name):
  client = MongoClient(conn_str)
  db = client.get_database(db_name)
  collection = db.get_collection(collection_name)
  return collection