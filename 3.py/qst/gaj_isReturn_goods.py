# coding:utf-8

import sys
import csv
sys.path.append("..\\utils")

from mongo import getCollection
from excel import getWantedField

connStr = "mongodb://"
dbName = "zght"
goodsCollectionName = "zght_goods"
gajCategoryCollectionName = "gaj_category"

goods_list = getCollection(connStr, dbName, goodsCollectionName)
gaj_category = getCollection(connStr, dbName, gajCategoryCollectionName)

csvFile = open("result.csv", "w", newline='', encoding="utf-8-sig")
writer = csv.writer(csvFile)

rows = []
results = []
header = ["商品编号", "商品名称", "三级类目"]
rows.append(header)

for goods in goods_list.find({
        "goods_info.isReturn": 1,
        "goods_info.state": 1,
        "u8id": {
            '$regex': "^G"
        }
}):
    results.append([
        goods['u8id'], goods['goods_info']['name'],
        goods['goods_info']['category'][2]
    ])

for item in results:
    category = gaj_category.find_one({'code': item[2]})
    item[2] = category['name']
    rows.append(item)

writer.writerows(rows)

csvFile.close()
