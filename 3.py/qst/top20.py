import sys
sys.path.append("../utils/")

from mongo import getCollection
from excel import readExcel

conn_str = "mongodb://192.168.6.232:27017"
db_name = "test"
top20_collection_name = "top20"

collection = getCollection(conn_str, db_name, top20_collection_name)

file_path = u"../qst/../前20.xlsx"

res = readExcel(file_path)

list = []

for row in res:
  obj = {
    'sku': str(row['U8ID']),
    'category': row['category'],
    'product': row['product'],
    'rank': int(row['rank'])
  }

  list.append(obj)

collection.insert_many(list)
