import sys
sys.path.append("..\\utils")
import json

from mongo import getCollection


def getInvoiceType(type):
    if (type == 1):
        return "普通发票"
    elif (type == 2):
        return "增值税普通发票"
    elif (type == 3):
        return "增值税专用发票"
    else:
        return ""


def getPaymentType(type):
    if (type == 0):
        return "后付"
    elif (type == 1):
        return "货到付款"
    elif (type == 2):
        return "在线支付"
    else:
        return ""


connStr = ""
orders = getCollection(connStr, "qst_eb", "order")
cndjOrders = getCollection(connStr, "qst", "cndj_order")

ebOids = [
    ebOrder['order']['ebOid']
    for ebOrder in orders.find({
        "order.platformType": "0030"
    })
]

for order in cndjOrders.find({"oid": {'$in': ebOids}}):
    orderInfo = order['order']
    extra = {}
    extra['付款类型'] = getPaymentType(orderInfo['paymentType'])
    extra['发票类型'] = getInvoiceType(orderInfo['invoiceType'])
    extra['发票抬头类型'] = '个人' if orderInfo['selectedInvoiceTitle'] == 1 else '单位'
    extra['发票抬头'] = orderInfo['companyName']
    extra['发票内容'] = orderInfo['invoiceContent']
    extra['订单含税总金额'] = orderInfo['orderPrice']
    extra['订单不含税总金额'] = orderInfo['orderNakedPrice']
    extra['订单税额'] = orderInfo['orderTaxPrice']
    extra['纳税人识别号'] = orderInfo['taxNo']
    extra['开票地址'] = orderInfo['addressTel']
    extra['开户行'] = orderInfo['bankInfo']

    result = orders.update_one({
        "order.ebOid": order['oid']
    }, {'$set': {
        'extraData': json.dumps(extra, ensure_ascii=False)
    }})
    if (result.matched_count) > 0:
        print('更新成功' + order['oid'])
