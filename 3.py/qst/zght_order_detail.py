# coding:utf-8

import sys
sys.path.append("..\\utils")

from excel import exportToXlsxFromDictList
from mongo import getCollection

connStr = "mongodb://"
dbName = "zght"
orderCollectionName = "zght_order"

orders = getCollection(connStr, dbName, orderCollectionName)

oids = []

results = []
for order in orders.find({ 'oid': { '$in': oids } }):
    for item in order['order']['sku']:
      temp = {}
      temp['oid'] =  order['oid']
      temp['customerOid'] = order['order']['thirdOrder']
      temp['sku'] = item['skuId']
      temp['name'] = item['name']
      temp['num'] = item['num']
      temp['price'] = item['price']
      results.append(temp)

columns = (
  '电商订单号',
  '客户订单号',
  '商品编号',
  '商品名称',
  '商品数量',
  '商品单价',
)

exportToXlsxFromDictList(columns, results)
