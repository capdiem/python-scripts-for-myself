# coding:utf-8

import sys
import csv
sys.path.append("..\\utils")

from mongo import getCollection
from excel import getWantedField

connStr = "mongodb://"
dbName = "zght"
goodsCollectionName = "zght_goods"

goods_list = getCollection(connStr, dbName, goodsCollectionName)

csvFile = open("result.csv", "w", newline='', encoding="utf-8-sig")
writer = csv.writer(csvFile)

rows = []
header = ["name", "categoryCode"]
rows.append(header)

for goods in goods_list.find({"goods_info.deliveryTime": 3}).limit(3000):
    rows.append([goods['goods_info']['name'], "C" + goods['category']])

writer.writerows(rows)

csvFile.close()
