#coding:utf-8

import sys
sys.path.append("..\\utils")

from mongo import getCollection
from excel import exportToXlsxFromDictList

connStr = "mongodb://192.168.8.60"
dbName = "zjj"
goodsCollectionName = "zght_goods"
categoryCollectionName = "zght_category"

goodsCollection = getCollection(connStr, dbName, goodsCollectionName)
categoryCollection = getCollection(connStr, dbName, categoryCollectionName)

results = []

for goods in goodsCollection.find({"goods_info.state": 1}):
    temp = {}
    temp['sku'] = goods['u8id']
    temp['name'] = goods['goods_info']['name']
    temp['saleUnit'] = goods['goods_info']['saleUnit']
    temp['price'] = goods['price_info']['ecPrice']

    categories = goods['goods_info']['category']
    if categories[0].count(','):
        categories = categories[0].split(',')

    for index, category in enumerate(categories):
        result = categoryCollection.find_one({'code': category})
        if result != None:
            temp['category' + str(index)] = result['name']

    results.append(temp)

columns = (
  '商品编号',
  '商品名称',
  '销售单位',
  '商品售价',
  '一级类目',
  '二级类目',
  '三级类目',
)

exportToXlsxFromDictList(columns, results)
