# coding:utf-8

import sys
import datetime
sys.path.append("..\\utils")

from constants import provinceDicts
from excel import exportToXlsxFromDictList
from mongo import getCollection


def getProvince(province=""):
    province = province.strip()
    if (province == ""):
        return ""

    province = province.replace("中国", "")

    if (province.__len__() < 2):
        return ""

    province = (province.strip())[0:2]

    value = provinceDicts.get(province, False)
    if (value == False):
        return ""
    else:
        return value


connStr = "mongodb://192.168.8.60:27017"
dbName = "zght"
orderCollectionName = "zght_order"
orderTrackingCollectionName = "zght_order_tracking"

orders = getCollection(connStr, dbName, orderCollectionName)
orderTrackings = getCollection(connStr, dbName, orderTrackingCollectionName)

results = []
for order in orders.find({
        # 'date': {
        #     '$lt': datetime.datetime(2019, 1, 1)
        # },
        # 'status.skuStatus': {
        #     # '$ne': 0
        #     '$eq': 1
        # }
        'order.address': {
            '$regex': "^浙江"
        }
}):
    temp = {}
    temp['oid'] = order['oid']
    temp['customerOid'] = order['order']['thirdOrder']
    temp['date'] = order['date'].strftime('%Y-%m-%d %H:%M:%S')
    # temp['province'] = getProvince(order['order']['address'])
    temp['address'] = order['order']['address']
    temp['customer'] = order['order'].get('submitOrg', '')
    # temp['sku'] = str(order['order']['sku'])
    temp['name'] = order['order']['name']
    temp['mobile'] = order['order']['mobile']
    temp['email'] = order['order']['email']
    temp['remark'] = order['order']['remark']
    temp['price'] = order['order']['orderPrice']

    # tracking = orderTrackings.find_one({"oid": order['oid']})
    # if (tracking == None):
    #     temp['packageDate'] = ''
    #     temp['logisticsDate'] = ''
    # else:
    #     packageTraces = list(
    #         filter(lambda u: u.get('status') == '拆分发货单', tracking['traces']))

    #     if (packageTraces.__len__() > 0):
    #         temp['packageDate'] = packageTraces[0]['time']
    #     else:
    #         temp['packageDate'] = ''

    #     logisticsTraces = list(
    #         filter(lambda u: u.get('status') == '订单妥投', tracking['traces']))
    #     if (logisticsTraces.__len__() > 0):
    #         temp['logisticsDate'] = logisticsTraces[0]['time']
    #     else:
    #         temp['logisticsDate'] = ''

    if (order['status']['orderStatus'] == 0):
        temp['status'] = '取消'
    elif (order['status']['logistStatus'] == 1):
        temp['status'] = '妥投'
    elif (order['status']['submitStatus'] == 0):
        if ((order['status']['skuStatus'] == 0
             or order['status']['skuStatus'] == 2)
                and (datetime.datetime.now() - order['date']).days > 7):
            temp['status'] = '取消'
        else:
            temp['status'] = '未确认'
    elif (order['status']['logistStatus'] == 0):
        temp['status'] = '未妥投'
    elif (order['status']['logistStatus'] == 2):
        temp['status'] = '拒妥'
    elif (order['status']['logistStatus'] == 3):
        temp['status'] = '部分妥投'
    else:
        temp['status'] = ''

    results.append(temp)

columns = (
    '电商订单号',
    '客户订单号',
    '下单时间',
    # '地区',
    '收货地址',
    '客户',
    # '商品详情',
    '收件人',
    '联系电话',
    '邮箱地址',
    '备注',
    '订单金额',
    # '发货时间',
    # '妥投时间',
    '订单状态')

exportToXlsxFromDictList(columns, results)
