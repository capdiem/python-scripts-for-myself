# coding:utf-8

import sys
import datetime
sys.path.append("..\\utils")

from excel import exportToXlsxFromDictList
from mongo import getCollection

connStr = "mongodb://"
dbName = "zght"
invoiceCollectionName = "zght_invoice"
orderCollectionName = "zght_order"

invoices = getCollection(connStr, dbName, invoiceCollectionName)
orders = getCollection(connStr, dbName, orderCollectionName)

results = []
for item in invoices.find({
        'status.code': '0004',
        'date': {
            '$gt': datetime.datetime(2019, 5, 15)
        }
}):
    for oid in item['InvoiceApplyInfo']['supplierOrder']:
        temp = {}
        temp['markId'] = item['InvoiceApplyInfo']['markId']
        temp['date'] = item['date'].strftime('%Y-%m-%d %H:%M:%S')
        temp['oid'] = oid

        order = orders.find_one({'oid': oid})
        temp['coid'] = order['order']['thirdOrder']

        results.append(temp)

columns = ('MARKID', '申请时间', '电商订单号', '客户订单号')

exportToXlsxFromDictList(columns, results)
