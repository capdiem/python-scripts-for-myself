# coding:utf-8

import sys
sys.path.append("..\\utils")

from excel import exportToXlsxFromDictList
from mongo import getCollection

connStr = "mongodb://192.168.8.60"
dbName = "zght"
orderCollectionName = "zght_order"

orders = getCollection(connStr, dbName, orderCollectionName)

results = []
for order in orders.find({ "status.skuStatus": 0, "status.orderStatus": 1, "status.submitStatus": 1 }):
    temp = {}
    temp['oid'] =  order['order']['thirdOrder']
    results.append(temp)

columns = (
  '客户订单号',
)

exportToXlsxFromDictList(columns, results)
